<?php

namespace Tests\Unit;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ChannelTest extends TestCase
{
    use RefreshDatabase;

    protected $channel;

    public function setUp() : void
    {
        parent::setUp();

        $this->channel = factory('App\Channel')->create();
    }

    /** @test */
    public function a_channel_has_threads()
    {
        $this->assertInstanceOf(Collection::class, $this->channel->threads);
    }
}
