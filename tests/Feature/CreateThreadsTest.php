<?php

namespace Tests\Feature;

use App\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function guests_may_not_create_threads()
    {
        $this->post('threads')
            ->assertRedirect('login');

        $this->get('threads/create')
            ->assertRedirect('login');
    }

    /** @test */
    public function an_authenticated_user_can_create_new_forum_threads()
    {
        $this->signIn();

        $thread = factory('App\Thread')->raw(['user_id' => auth()->id()]);

        $this->post('threads', $thread);

        $this->assertDatabaseHas('threads', $thread);
    }

    /** @test */
    public function a_thread_requires_a_title()
    {
        $this->signIn();

        $attributes = factory('App\Thread')->raw(['title' => '']);

        $this->post('threads', $attributes)->assertSessionHasErrors('title');
    }

    /** @test */
    public function a_thread_requires_a_body()
    {
        $this->signIn();

        $attributes = factory('App\Thread')->raw(['body' => '']);

        $this->post('threads', $attributes)->assertSessionHasErrors('body');
    }

    /** @test */
    public function a_thread_requires_a_valid_channel()
    {
        $this->signIn();

        factory('App\Channel', 2)->create();

        $this->post('threads', factory('App\Thread')->raw(['channel_id' => '']))
            ->assertSessionHasErrors('channel_id');

        $this->post('threads', factory('App\Thread')->raw(['channel_id' => 5]))
            ->assertSessionHasErrors('channel_id');
    }
}
