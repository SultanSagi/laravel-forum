<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Thread;
use Faker\Generator as Faker;

$factory->define(Thread::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->sentence(2),
        'user_id' => factory('App\User'),
        'channel_id' => factory('App\Channel'),
    ];
});
