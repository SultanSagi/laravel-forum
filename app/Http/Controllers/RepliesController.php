<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\Request;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store($channelId, Thread $thread)
    {
        $attr = request()->validate([
            'body' => 'required'
        ]);

        $thread->addReply($attr + ['user_id' => auth()->id()]);

        return back();
    }
}
