@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a new thread</div>

                    <div class="card-body">
                        <form action="/threads" method="post">
                            @csrf

                            <div class="form-group">
                                <input type="text" class="form-control" name="title" placeholder="Title">
                            </div>

                            <div class="form-group">
                                <textarea name="body" id="body" class="form-control" placeholder="Body" rows="5"></textarea>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-default">Post</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
