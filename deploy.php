<?php
namespace Deployer;

require 'recipe/laravel.php';

// Название проекта
set('application', 'notifyer');

// Репозиторий проекта
set('repository', 'https://gitlab+deploy-token-10:Czi7t3dLAwXxHLQzYYcd@gitlab.com/SultanSagi/laravel-forum.git');

// [Optional] Allocate tty for git clone. Default value is false.
//set('git_tty', true);

// Общие файлы/директории которые перемещаются из релиза в релиз.
add('shared_files', ['.env']);

// Директории в которые может писать веб-сервер.
add('writable_dirs', ['storage', 'vendor']);
set('allow_anonymous_stats', false);

// @todo
host('testing')
    ->hostname('185.146.3.235')
    ->set('branch', 'master')
    ->stage('testing')
    ->user('deployer')
    ->set('deploy_path', '/var/www/halyk.ibec.systems');

// Build
task('build', function () {
    run('cd {{release_path}} && build');
});

// Заюзаем сиды если они добавлены
desc('Execute artisan db:seed');
task('artisan:db:seed', function () {
    $output = run('{{bin/php}} {{release_path}}/artisan db:seed --force');
    writeln('<info>' . $output . '</info>');
});

// Сделаем composer dump-autoload
desc('Execute composer dump-autoload');
task('composer:dump-autoload', function() {
    run('cd {{release_path}} && composer dump-autoload --verbose');
});

// После дэплоя пакетов запустим db:seed
after('deploy:vendors', 'artisan:db:seed');

// После db:seed запустим composer dump-autoload
after('artisan:db:seed', 'composer:dump-autoload');

// Удалим багнутный конфиг с Closure
task('deploy:writable', function() {
    run('echo do not writable');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
before('deploy:symlink', 'artisan:migrate');

